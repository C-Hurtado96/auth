const mix = require('laravel-mix');

mix.setPublicPath('public/auth/');

/*
|--------------------------------------------------------------------------
| SASS
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| CSS
|--------------------------------------------------------------------------
*/
mix.styles([
        'resources/assets/auth/css/bootstrap.min.css',
    ],
    'public/auth/css/custom.min.css')
    .version();

mix.styles([
        'resources/assets/auth/css/style.css',
        'resources/assets/auth/css/skins/orange.css'
    ],
    'public/auth/css/vendor.min.css')
    .version();


/*
 |--------------------------------------------------------------------------
 | JS
 |--------------------------------------------------------------------------
 */


mix.scripts([
        'resources/assets/auth/js/modernizr.js',
    ],
    'public/auth/js/modernizr.min.js')
    .extract()
    .version()

mix.scripts([
        'resources/assets/auth/js/jquery-2.2.4.min.js',
        'resources/assets/auth/js/plugins/bootstrap.min.js',
        'resources/assets/auth/js/plugins/jquery.easing.1.3.js',
        'resources/assets/auth/js/custom.js',
    ],
    'public/auth/js/script.min.js')
    .extract()
    .version()
