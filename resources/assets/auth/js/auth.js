import {swalError, swalSuccessNoButton} from "../../commons/js/core";

class Auth {
    // Login users
    static login() {
        let $button = $('#login');
        $button.click(function() {
            let $form = $('#login-form');
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $form.serialize()
            }).done(function(json){
                swalSuccessNoButton(json);
                setTimeout(() => window.location.href = json.route, 1000);

            }).fail(function(json){
                 swalError(json);
                 setTimeout(() => window.location.href = json.route='', 1000);
            }).always(function(){

            });
        });

        $('#login-form').keypress(function (event) {
            if (event.keyCode === 13) {
                $button.click();
            }
        });
    }
}
window.Auth = Auth;
