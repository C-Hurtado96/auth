<html lang="es">
<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.png">

    <!-- Template CSS Files -->
    <link rel="stylesheet" href="{{ mix('css/custom.min.css','auth') }}">
    <link rel="stylesheet" href="{{ asset('assets/auth/css/font-awesome.min.css')}} ">
    <link rel="stylesheet" href="{{ mix('css/vendor.min.css','auth')}} "/>
    <!-- Live Style Switcher - demo only -->

    <!-- Template JS Files -->
    <script src="{{ mix('js/modernizr.min.js','auth') }}"></script>
</head>

<body class="double-diagonal dark auth-page">
<!-- Preloader Starts -->
<div class="preloader" id="preloader">
    <div class="logopreloader">
        <img src="{{asset('img/logos/orange.png')}}" alt="logo-black">
    </div>
    <div class="loader" id="loader"></div>
</div>
<!-- Preloader Ends -->

<!-- Live Style Switcher Ends - demo only -->
<!-- Page Wrapper Starts -->
<div class="wrapper">
    <div class="container-fluid user-auth">
        <div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
            <!-- Logo Starts -->
            <a class="logo" href="index-kenburns.html">
                <img id="single-logo" class="img-responsive" src="{{asset('img/logos/orange.png')}}" alt="logo">
            </a>
            <!-- Logo Ends -->
            <!-- Slider Starts -->
            <div id="carousel-testimonials" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Indicators Starts -->
                <!--<ol class="carousel-indicators">
                    <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                    <li data-target="#carousel-testimonials" data-slide-to="2"></li>
                </ol>-->
                <!-- Indicators Ends -->
                <!-- Carousel Inner Starts -->
                <div class="carousel-inner">
                    <!-- Carousel Item Starts -->
                    <div class="item active item-1">
                        <div>
                            <blockquote>
                                <p>Amira's Team Was Great To Work With And Interpreted Our Needs Perfectly.</p>
                                <footer><span>Lucy Smith</span>, England</footer>
                            </blockquote>
                        </div>
                    </div>
                    <!-- Carousel Item Ends -->
                    <!-- Carousel Item Starts -->
                    <!--<div class="item item-2">
                        <div>
                            <blockquote>
                                <p>The Team Is Endlessly Helpful, Flexible And Always Quick To Respond, Thanks Amira!</p>
                                <footer><span>Rawia Chniti</span>, Russia</footer>
                            </blockquote>
                        </div>
                    </div>-->
                    <!-- Carousel Item Ends -->
                    <!-- Carousel Item Starts -->
                    <!--<div class="item item-3">
                        <div>
                            <blockquote>
                                <p>We Are So Appreciative Of Their Creative Efforts, And Love Our New Site!, millions of thanks Amira!</p>
                                <footer><span>Mario Verratti</span>, Spain</footer>
                            </blockquote>
                        </div>
                    </div>-->
                    <!-- Carousel Item Ends -->
                </div>
                <!-- Carousel Inner Ends -->
            </div>
            <!-- Slider Ends -->
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <!-- Logo Starts -->
            <a class="visible-xs" href="index-kenburns.html">
                <img id="logo-mobile-light" class="img-responsive mobile-logo white-l" src="{{asset('img/styleswitcher/logos/yellow.png')}}" alt="logo">
                <img id="logo-mobile-dark" class="img-responsive mobile-logo dark-l" src="{{asset('img/styleswitcher/logos/logos-dark/yellow.png')}}" alt="logo">
            </a>
            <!-- Logo Ends -->
            <div class="form-container">
                <div>
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span>member</span> login</h1>
                        <p>great to have you back!</p>
                    </div>
                    <!-- Main Heading Ends -->
                    <!-- Form Starts -->
                    <form class="custom-form" action="" id="data_form">
                        <!-- Input Field Starts -->
                        <div class="form-group">
                            <input class="form-control" name="username" id="username" placeholder="Usename" type="text" required>
                        </div>
                        <!-- Input Field Ends -->
                        <!-- Input Field Starts -->
                        <div class="form-group">
                            <input class="form-control" name="password" id="password" placeholder="Password" type="password" required>
                        </div>
                        <!-- Input Field Ends -->
                        <!-- Submit Form Button Starts -->
                        <div class="form-group">
                            <button class="custom-button login" type="button" id='login'>login</button>
                        </div>
                        <!-- Submit Form Button Ends -->
                    </form>
                    <!-- Form Ends -->
                </div>
            </div>
            <!-- Copyright Text Starts -->
            <p class="text-center copyright-text">Copyright © 2018 Amira All Rights Reserved</p>
            <!-- Copyright Text Ends -->
        </div>
    </div>
</div>
<!-- Wrapper Ends -->
<!-- Template JS Files -->
    <script src="{{ mix('js/script.min.js','auth')}}"></script>

</body>
</html>
